-- common variables
local menuFrame = CreateFrame("Frame", "STW_Menu", UIParent, "MainMenuTemplate")
local entries = {}
local frameCache = {}
local buttonCache = {}
local pendingRolls = {}
local pendingDeletion = {}
local pendingButtonCreations = {}
local pendingButtonUpdates = {}
local pendingItemLinks = {}
local windowHeight = 400
local windowWidth = 800
local entryHeight = 60
local updateInterval = 1
local addonPrefix = "stw_addonmsg"
local loadtime = 0

local classColors = {
".95:.9:.6", -- warrior
"1:.41:.7", -- paladin
".19:.8:.19", -- hunter
"1:.96:.41", -- rogue
"1:1:1", -- priest
"1:0:0", -- death knight
"0:.5:1", -- shaman
"0:.82:1", -- mage
".5:.32:.55", -- warlock
".52:1:.52", -- monk
"1:.61:0", -- druid
".78:.26:.99" -- demon hunter
}
local itemColors = {
"1:1:1", -- common
"0:1:0", -- uncommon
"0:.5:1", -- rare
".4:0:.8", -- epic
"1:.61:0" -- legendary
}
local equipSlots = {
["INVTYPE_AMMO"] = "0",
["INVTYPE_HEAD"] = "1",
["INVTYPE_NECK"] = "2", 
["INVTYPE_SHOULDER"] = "3",
["INVTYPE_BODY"] = "4",
["INVTYPE_CHEST"] = "5",
["INVTYPE_ROBE"] = "5",
["INVTYPE_WAIST"] = "6",
["INVTYPE_LEGS"] = "7",
["INVTYPE_FEET"] = "8",
["INVTYPE_WRIST"] = "9",
["INVTYPE_HAND"] = "10",
["INVTYPE_FINGER"] = "11%12",
["INVTYPE_TRINKET"] = "13%14",
["INVTYPE_CLOAK"] = "15",
["INVTYPE_WEAPON"] = "16%17",
["INVTYPE_SHIELD"] = "17",
["INVTYPE_2HWEAPON"] = "16",
["INVTYPE_WEAPONMAINHAND"] = "16",
["INVTYPE_WEAPONOFFHAND"] = "17",
["INVTYPE_HOLDABLE"] = "17",
["INVTYPE_RANGED"] = "18",
["INVTYPE_THROWN"] = "18",
["INVTYPE_RANGEDRIGHT"] = "18",
["INVTYPE_RELIC"] = "18",
["INVTYPE_TABARD"] = "19"
}
local quotes = {
	"\"Nothing is yours. It is to use. It is to share.\nIf you will not share it, you cannot use it.\"",
	"\"Sharing is a much better way to communicate than proving.\"",
	"\"Share your life with others.\nYou will have a joyful life.\"",
	"\"By sharing, we stand prepared to build relationships\nand give wings to humanity.\"",
	"\"If people truly cared and shared equally,\nthe world would have no poverty at all.\"",
	"\"Sharing makes you bigger than you are. The more you pour out,\nthe more life will be able to pour in.\"",
	"\"We are not cisterns made for hoarding,\nwe are channels made for sharing.\""
}

-- frame management for entries to limit memory consumption
local function AcquireFrame(parent)
	local frame = tremove(frameCache) or CreateFrame("Frame")
	frame:SetParent(parent)
	return frame
end
local function ReleaseFrame(frame)
	frame:Hide()
	frame:SetParent(nil)
	frame:ClearAllPoints()
	tinsert(frameCache, frame)
end
local function AcquireButton(parent)
	local frame = tremove(buttonCache) or CreateFrame("Button", nil, parent, "SecureActionButtonTemplate")
	frame:SetParent(parent)
	return frame
end
local function ReleaseButton(frame)
	frame:Hide()
	frame:SetParent(nil)
	frame:ClearAllPoints()
	tinsert(buttonCache, frame)
end

-- round numbers to nearest integer
local function round(num, numDecimalPlaces)
	local mult = 10^(numDecimalPlaces or 0)
	return math.floor(num * mult + 0.5) / mult
end

-- reposition MiniMap button
local function STW_MinimapButton_Reposition()
	STW_MinimapButton:SetPoint("TOPLEFT","Minimap","TOPLEFT",52-(80*cos(STW_Config["MinimapPos"])),(80*sin(STW_Config["MinimapPos"]))-52)
end

-- minimap button dragging update function
function STW_MinimapButton_DraggingFrame_OnUpdate()
	local xpos,ypos = GetCursorPosition()
	local xmin,ymin = Minimap:GetLeft(), Minimap:GetBottom()
	xpos = xmin-xpos/UIParent:GetScale()+70
	ypos = ypos/UIParent:GetScale()-ymin-70
	local value = math.deg(math.atan2(ypos,xpos))
	if value < 0 then 
		value = value + 360
	end
	STW_Config["MinimapPos"] = value
	Minimap_Slider1:SetValue(value)
	STW_MinimapButton_Reposition() -- move the button
end

-- minimap button slider update function
function STW_MinimapButton_Slider_OnUpdate(value)
	STW_Config["MinimapPos"] = value
	STW_MinimapButton_Reposition() -- move the button
end

-- frame scale slider update function
function STW_FrameScale_Slider_OnUpdate(value)
	STW_Config["FrameScale"] = value
	menuFrame:SetScale(value)
end

-- Config Frame 'Okay' button
function STW_ConfigFrame_Okay(panel)
	-- Escape to close menu frame
	if Gen_Check1:GetChecked() then
		STW_Config["MenuEscape"] = "true"
		tinsert(UISpecialFrames, menuFrame:GetName())
	else
		STW_Config["MenuEscape"] = "false"
	end
	-- Hide quote string
	if Gen_Check2:GetChecked() then
		STW_Config["HideQuote"] = "true"
	else
		STW_Config["HideQuote"] = "false"
	end
	-- Disable minimap button loot noise
	if Gen_Check3:GetChecked() then
		STW_Config["DisableLootNoise"] = "true"
	else
		STW_Config["DisableLootNoise"] = "false"
	end
	-- Free minimap button
	if Minimap_Check2:GetChecked() then
		STW_Config["FreeMinimap"] = "true"
		Minimap_Slider1_Editbox:Disable()
		STW_MinimapButton:SetScript("OnDragStart", function()
			STW_MinimapButton:LockHighlight()
			STW_MinimapButton:StartMoving()
		end)
		STW_MinimapButton:SetScript("OnDragStop", function()
			STW_MinimapButton:UnlockHighlight()
			STW_MinimapButton:StopMovingOrSizing()
		end)
	else
		STW_Config["FreeMinimap"] = "false"
		Minimap_Slider1_Editbox:Enable()
		STW_MinimapButton:ClearAllPoints()
		STW_MinimapButton_Reposition()
		STW_MinimapButton:SetScript("OnDragStart", function()
			STW_MinimapButton:LockHighlight()
			STW_MinimapButton_DraggingFrame:Show()
		end)
		STW_MinimapButton:SetScript("OnDragStop", function()
			STW_MinimapButton:UnlockHighlight()
			STW_MinimapButton_DraggingFrame:Hide()
		end)
	end
	-- Lock minimap button
	if Minimap_Check1:GetChecked() then
		STW_Config["LockMinimap"] = "true"
		STW_MinimapButton:RegisterForDrag()
		Minimap_Slider1_Editbox:Disable()
	else
		STW_Config["LockMinimap"] = "false"
		STW_MinimapButton:RegisterForDrag("LeftButton","RightButton")
		Minimap_Slider1_Editbox:Enable()
	end
	-- Hide minimap button
	if Minimap_Check3:GetChecked() then
		STW_Config["HideMinimap"] = "true"
		STW_MinimapButton:Hide()
	else
		STW_Config["HideMinimap"] = "false"
		STW_MinimapButton:Show()
	end
end

-- Config Frame 'Cancel' button, also loads saved vars
function STW_ConfigFrame_Cancel(panel)
	-- Settings table
	if not STW_Config then
		STW_Config = {}
	end
	-- Escape button to close main menu
	if not STW_Config["MenuEscape"] then
		STW_Config["MenuEscape"] = "true"
		Gen_Check1:SetChecked(true)
	else
		if STW_Config["MenuEscape"] == "true" then
			Gen_Check1:SetChecked(true)
		else
			Gen_Check1:SetChecked(false)
		end
	end
	-- Hide quote string
	if not STW_Config["HideQuote"] then
		STW_Config["HideQuote"] = "false"
		Gen_Check2:SetChecked(false)
	else
		if STW_Config["HideQuote"] == "true" then
			Gen_Check2:SetChecked(true)
		else
			Gen_Check2:SetChecked(false)
		end
	end
	-- Disable minimap button loot noise
	if not STW_Config["DisableLootNoise"] then
		STW_Config["DisableLootNoise"] = "false"
		Gen_Check3:SetChecked(false)
	else
		if STW_Config["DisableLootNoise"] == "true" then
			Gen_Check3:SetChecked(true)
		else
			Gen_Check3:SetChecked(false)
		end
	end
	-- Frame scale slider
	if not STW_Config["FrameScale"] then
		STW_Config["FrameScale"] = 1
		Gen_Slider1:SetValue(STW_Config["FrameScale"])
	else
		Gen_Slider1:SetValue(STW_Config["FrameScale"])
		STW_FrameScale_Slider_OnUpdate(STW_Config["FrameScale"])
	end
	-- Lock minimap
	if not STW_Config["LockMinimap"] then
		STW_Config["LockMinimap"] = "false"
		Minimap_Check1:SetChecked(false)
	else
		if STW_Config["LockMinimap"] == "true" then
			Minimap_Check1:SetChecked(true)
		else
			Minimap_Check1:SetChecked(false)
		end
	end
	-- Free minimap
	if not STW_Config["FreeMinimap"] then
		STW_Config["FreeMinimap"] = "false"
		Minimap_Check2:SetChecked(false)
	else
		if STW_Config["FreeMinimap"] == "true" then
			Minimap_Check2:SetChecked(true)
		else
			Minimap_Check2:SetChecked(false)
		end
	end
	-- Hide minimap
	if not STW_Config["HideMinimap"] then
		STW_Config["HideMinimap"] = "false"
		Minimap_Check3:SetChecked(false)
	else
		if STW_Config["HideMinimap"] == "true" then
			Minimap_Check3:SetChecked(true)
		else
			Minimap_Check3:SetChecked(false)
		end
	end
	-- Minimap button positioning
	if not STW_Config["MinimapPos"] then
		STW_Config["MinimapPos"] = 340
		Minimap_Slider1:SetValue(STW_Config["MinimapPos"])
		if STW_Config["FreeMinimap"] == "false" then
			STW_MinimapButton_Reposition()
		end
	else
		Minimap_Slider1:SetValue(STW_Config["MinimapPos"])
		if STW_Config["FreeMinimap"] == "false" then
			STW_MinimapButton_Reposition()
		end
	end
end

-- formats time in seconds into 00:00 format
local function GetPrettyTime(timeInSeconds)
	timeInSeconds = math.abs(tonumber(timeInSeconds))
	local minutes = math.floor(timeInSeconds/60)
	if minutes < 10 then minutes = "0" .. minutes end
	local seconds = timeInSeconds - minutes*60
	if seconds < 10 then seconds = "0" .. seconds end
	local prettyTime = minutes .. ":" .. seconds

	return prettyTime
end

-- find entry in table by custom item string in format "ownerName%ownerRealm%itemLink"
-- returns table index
-- returns zero if not found
local function FindEntry(itemString)
	local ownerName, ownerRealm, itemLink = strsplit("%", itemString)
	local index = 0
	
	if table.getn(entries) > 0 then 
		for i=1, table.getn(entries), 1 do
			if ownerName == entries[i].ownerEntry.ownerName
			and ownerRealm == entries[i].ownerEntry.ownerRealm
			and itemLink == entries[i].itemEntry.itemLink then
				index = i
				break
			end
		end
	end
	return index
end

-- check if item can be traded 
-- returns boolean, and bag, slot of tradeable item, or negative if no tradeable item found
local function IsTradeable(itemLink)     
	-- verify item exists in bags first
	local bag, slot
	for i = 0, NUM_BAG_SLOTS do -- NUM_BAG_SLOTS is an API constant
		for j = 1, GetContainerNumSlots(i) do
			if GetContainerItemLink(i, j) == itemLink then
				-- matching link found
				bag, slot = i, j
				TooltipScanner:SetBagItem(bag, slot)
				local tipRegion, tipText
				-- check if tooltip has the tradeable message
				for k = 1, TooltipScanner:NumLines() do
					tipRegion = _G["TooltipScannerTextLeft" .. k]
					tipText = tipRegion:GetText()
					if string.find(tipText, "You may trade this item") then
						return true, bag, slot
					end
				end
				-- check if tooltip has the BoE message
				for k = 1, TooltipScanner:NumLines() do 
					tipRegion = _G["TooltipScannerTextLeft" .. k]
					tipText = tipRegion:GetText()
					if string.find(tipText, "Binds when equipped") then
						return true, bag, slot
					end
				end
			end
		end
	end
	bag, slot = -1, -1
	return false, bag, slot
end

-- entry frame deletion by custom item string in format "ownerName%ownerRealm%itemLink"
local function DeleteByItem(itemString)
	local index = FindEntry(itemString)
			
	-- remove entry, move frames upwards
	if index > 0 then
		-- release buttons
		ReleaseButton(entries[index].ownerEntry.targetButton)
		ReleaseButton(entries[index].winningEntry.targetButton)
		entries[index].ownerEntry.targetButton = nil
		entries[index].winningEntry.targetButton = nil
		-- release frame
		ReleaseFrame(entries[index])
		-- re-anchor leftover entries
		if index == table.getn(entries) then -- check for last entry, no move required
			table.remove(entries, index)
		else
			table.remove(entries, index)
			for i=index, table.getn(entries), 1 do
				if i==1 then
					entries[i]:SetPoint("TOPLEFT", menuFrame.scrollFrame.scrollContent)
				else
					entries[i]:SetPoint("TOPLEFT", entries[i-1], "BOTTOMLEFT")
				end
			end
		end
		-- adjust scrollbar
		numEntries = table.getn(entries)
		if menuFrame.scrollBar.max > 0 then
			menuFrame.scrollBar.max = menuFrame.scrollBar.max - entryHeight
			menuFrame.scrollBar:SetMinMaxValues(0, menuFrame.scrollBar.max)
			menuFrame.scrollFrame:SetScript("OnMouseWheel", function(self, delta) 
				local curValue = menuFrame.scrollBar:GetValue()
			
				if IsShiftKeyDown() and (delta > 0) then
					menuFrame.scrollBar:SetValue(0)
				elseif IsShiftKeyDown() and (delta < 0) then
					menuFrame.scrollBar:SetValue(menuFrame.scrollBar.max)
				elseif (delta < 0) and (curValue < menuFrame.scrollBar.max) then
					menuFrame.scrollBar:SetValue(curValue + 20)
				elseif (delta > 0) and (curValue > 1) then
					menuFrame.scrollBar:SetValue(curValue - 20)
				end
			end)
		end
	end
end

-- attempt to roll on an item
-- custom item string in format "ownerName%ownerRealm%itemLink"
-- custom roll string in format "rollName%rollRealm%rollClass%roll"
local function UpdateRoll(itemString, rollString)
	local index = FindEntry(itemString)
	if index > 0 then
		local entryFrame = entries[index]
		local highestRoll = tonumber(entryFrame.winningEntry.roll)
		local highestName = entryFrame.winningEntry.name
		local highestRealm = entryFrame.winningEntry.realm
		local ownerName, ownerRealm, itemLink = strsplit("%", itemString)
		local rollName, rollRealm, rollClass, roll = strsplit("%", rollString)
		roll = tonumber(roll)
		
		-- check highest roll
		if roll > highestRoll then
			C_ChatInfo.SendAddonMessage(addonPrefix, "U%"..rollName.."%"..rollRealm.."%"..rollClass.."%"..roll.."%"..ownerName.."%"..ownerRealm.."%"..itemLink, "RAID")
		end
	end
end

-- adjust the timer of an entry frame 
local function AdjustTimer(entryFrame, seconds)
	entryFrame.statusEntry.timer.time = tonumber(entryFrame.statusEntry.timer.time) - tonumber(seconds)
	entryFrame.statusEntry.timeText:SetText(GetPrettyTime(entryFrame.statusEntry.timer.time))
end

-- update only the status of an entry frame
local function CloseEntryStatus(entryFrame, statusTime)
	entryFrame.statusEntry.text:SetTextColor(1, 0, 0)
	entryFrame.statusEntry.text:SetText("Ended")
	entryFrame.statusEntry.status = "Ended"
	entryFrame.statusEntry.timer.time = statusTime
	entryFrame.statusEntry.timeText:SetText(GetPrettyTime(statusTime))
	entryFrame.statusEntry.statusButtonOpen:Hide()
	entryFrame.statusEntry.statusButtonEnded:Show()
	entryFrame.ownerEntry.endButton:Hide()
end

-- update entry info
local function UpdateEntry(entryFrame, ownerName, ownerRealm, ownerClass, itemLink, status, statusTime)
	local r, g, b -- for colors
	local playerName, _ = UnitName("player")
	local playerRealm = GetRealmName()
	
	-- set owner color
	r, g, b = strsplit(":", classColors[tonumber(ownerClass)])
	entryFrame.ownerEntry.text:SetTextColor(r, g, b)
	-- set owner info
	entryFrame.ownerEntry.ownerName = ownerName -- for future reference
	entryFrame.ownerEntry.ownerRealm = ownerRealm -- for future reference
	entryFrame.ownerEntry.text:SetText(ownerName .. "\n" .. ownerRealm)
	-- secure owner target button
	if not InCombatLockdown() then
		if(ownerRealm == playerRealm) then
			entryFrame.ownerEntry.targetButton:SetAttribute("macrotext", "/targetexact " .. ownerName)
		else
			entryFrame.ownerEntry.targetButton:SetAttribute("macrotext", "/targetexact " .. ownerName.."-"..ownerRealm:gsub("%s+", ""))
		end
		-- trade owner
		if(ownerRealm == playerRealm) then
			entryFrame.ownerEntry.targetButton:SetScript('OnMouseUp', function(self, button)
				if button == "RightButton" then
					InitiateTrade(ownerName)
				end
			end)
		else
			entryFrame.ownerEntry.targetButton:SetScript('OnMouseUp', function(self, button)
				if button == "RightButton" then
					InitiateTrade(ownerName.."-"..ownerRealm:gsub("%s+", ""))
				end
			end)
		end
	end
	-- show appropriate 'hide' or 'end' button
	if (ownerName == playerName) and (ownerRealm == playerRealm) then
		entryFrame.ownerEntry.endButton:Show()
	else
		entryFrame.ownerEntry.hideButton:Show()
	end
	-- 'hide' button script
	entryFrame.ownerEntry.hideButton:SetScript('OnClick', function(self, button)
		local dialog = StaticPopup_Show("STW_CONFIRM_HIDE")
		if (dialog) then
			dialog.data = ownerName .. "%" .. ownerRealm .. "%" .. itemLink
		end
	end)
	-- 'end' button script
	entryFrame.ownerEntry.endButton:SetScript('OnClick', function(self, button)
		local dialog = StaticPopup_Show("STW_CONFIRM_END")  
		if (dialog) then
			dialog.data = ownerName .. "%" .. ownerRealm .. "%" .. itemLink
		end
	end)
	
	-- set item info
	entryFrame.itemEntry.itemLink = itemLink -- for future reference
	local itemName, _, itemRarity, _, _, itemType,
	itemSubType, _, itemEquipLoc, _, _ =
    GetItemInfo("\"" .. itemLink .. "\"")
	local itemLevel, _, _ = GetDetailedItemLevelInfo(itemLink)	
	local _, _, _, _, itemID, _, _, _, _, _,
    _, _, _, _ = string.find(itemLink,"|?c?f?f?(%x*)|?H?([^:]*):?(%d+):?(%d*):?(%d*):?(%d*):?(%d*):?(%d*):?(%-?%d*):?(%-?%d*):?(%d*):?(%d*):?(%-?%d*)|?h?%[?([^%[%]]*)%]?|?h?|?r?")
	itemID = tonumber(itemID)
	local itemSlot = itemEquipLoc
	
	if itemName ~= nil then -- item is cached, proceed
		-- item color info, name
		if not itemRarity then 
			r, g, b = 1, 1, 1
		elseif itemRarity > 5 or itemRarity < 1 then
			r, g, b = 1, 1, 1
		else
			r, g, b = strsplit(":", itemColors[itemRarity])
		end
		entryFrame.itemEntry.text:SetTextColor(r, g, b)
		entryFrame.itemEntry.text:SetText("[" .. itemName .. "]")
	
		-- item link mouseover
		entryFrame.itemEntry:SetScript('OnEnter', function()
			-- add frame highlight
			entryFrame.itemEntry:SetBackdrop({edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 16})
			entryFrame.itemEntry:SetBackdropBorderColor(1, 1, 0)
			ItemTooltip1:ClearLines()	
			ItemTooltip1:SetHyperlink(itemLink)
			ItemTooltip1:SetPoint("BOTTOMLEFT", entryFrame.itemEntry, "TOPLEFT")
			ItemTooltip1:Show()
		end)
		
		-- item link shift modifier (view currently equipped)
		entryFrame.itemEntry:SetScript("OnEvent", function(self, event, key, state)
			if event == "MODIFIER_STATE_CHANGED" and ((key == "LSHIFT") or (key == "RSHIFT")) and entryFrame.itemEntry:IsMouseOver() and menuFrame:IsVisible() then
				if state == 1 then
					if equipSlots[itemSlot] then
						local charItem1, charItem2 = strsplit("%", equipSlots[itemSlot])
						charItem1 = tonumber(charItem1)
						charItem2 = tonumber(charItem2)
						if charItem1 then
							ItemTooltip2:ClearLines()	
							ItemTooltip2:SetHyperlink(GetInventoryItemLink("player", charItem1))
							ItemTooltip2:AddLine("Currently Equipped", .5, 1, 0)
							ItemTooltip2:SetPoint("BOTTOMLEFT", ItemTooltip1, "BOTTOMRIGHT")
							ItemTooltip2:Show()
						end
						if charItem2 then
							ItemTooltip3:ClearLines()	
							ItemTooltip3:SetHyperlink(GetInventoryItemLink("player", charItem2))
							ItemTooltip3:AddLine("Currently Equipped", .5, 1, 0)
							ItemTooltip3:SetPoint("BOTTOMLEFT", ItemTooltip2, "BOTTOMRIGHT")
							ItemTooltip3:Show()
						end
					elseif itemSubType == "Artifact\nRelic" then
						local _, _, relicType0, _ = C_ArtifactUI.GetRelicInfoByItemID(itemID)
						local _, _, relicType1, relicLink1 = C_ArtifactUI.GetEquippedArtifactRelicInfo(1)
						local _, _, relicType2, relicLink2 = C_ArtifactUI.GetEquippedArtifactRelicInfo(2)
						local _, _, relicType3, relicLink3 = C_ArtifactUI.GetEquippedArtifactRelicInfo(3)
						if relicType0 == relicType1 then
							if not ItemTooltip2:IsVisible() then
								ItemTooltip2:ClearLines()	
								ItemTooltip2:SetHyperlink(relicLink1)
								ItemTooltip2:AddLine("Currently Equipped", .5, 1, 0)
								ItemTooltip2:SetPoint("BOTTOMLEFT", ItemTooltip1, "BOTTOMRIGHT")
								ItemTooltip2:Show()
							else
								ItemTooltip3:ClearLines()	
								ItemTooltip3:SetHyperlink(relicLink1)
								ItemTooltip3:AddLine("Currently Equipped", .5, 1, 0)
								ItemTooltip3:SetPoint("BOTTOMLEFT", ItemTooltip2, "BOTTOMRIGHT")
								ItemTooltip3:Show()
							end
						end
						if relicType0 == relicType2 then
							if not ItemTooltip2:IsVisible() then
								ItemTooltip2:ClearLines()	
								ItemTooltip2:SetHyperlink(relicLink2)
								ItemTooltip2:AddLine("Currently Equipped", .5, 1, 0)
								ItemTooltip2:SetPoint("BOTTOMLEFT", ItemTooltip1, "BOTTOMRIGHT")
								ItemTooltip2:Show()
							else
								ItemTooltip3:ClearLines()	
								ItemTooltip3:SetHyperlink(relicLink2)
								ItemTooltip3:AddLine("Currently Equipped", .5, 1, 0)
								ItemTooltip3:SetPoint("BOTTOMLEFT", ItemTooltip2, "BOTTOMRIGHT")
								ItemTooltip3:Show()
							end
						end
						if relicType0 == relicType3 then
							if not ItemTooltip2:IsVisible() then
								ItemTooltip2:ClearLines()	
								ItemTooltip2:SetHyperlink(relicLink3)
								ItemTooltip2:AddLine("Currently Equipped", .5, 1, 0)
								ItemTooltip2:SetPoint("BOTTOMLEFT", ItemTooltip1, "BOTTOMRIGHT")
								ItemTooltip2:Show()
							else
								ItemTooltip3:ClearLines()	
								ItemTooltip3:SetHyperlink(relicLink3)
								ItemTooltip3:AddLine("Currently Equipped", .5, 1, 0)
								ItemTooltip3:SetPoint("BOTTOMLEFT", ItemTooltip2, "BOTTOMRIGHT")
								ItemTooltip3:Show()
							end
						end
					end
				else
					ItemTooltip2:Hide()
					ItemTooltip2:SetOwner(WorldFrame, "ANCHOR_NONE")
					ItemTooltip3:Hide()
					ItemTooltip3:SetOwner(WorldFrame, "ANCHOR_NONE")
				end
			end
		end)
		
		-- item link click functions
		entryFrame.itemEntry:SetScript('OnMouseUp', function(self, button)
			-- item link add to chat box
			if button == "LeftButton" and IsShiftKeyDown() and ChatFrame1EditBox:HasFocus() then
				ChatFrame1EditBox:Insert(itemLink)
			end
			-- open transmog dressing room
			if button == "LeftButton" and IsControlKeyDown() then
				DressUpItemLink(itemLink)
			end
		end)
	
		-- item level info
		entryFrame.ilvlEntry.text:SetText(itemLevel)
		
		-- formatted item type info
		-- change to "misc." to fit frame
		if itemSubType == "Miscellaneous" then
			itemSubType = "Misc."
		end
		-- modify to fit frame
		if itemSubType == "Artifact Relic" then
			itemSubType = "Artifact\nRelic"
		end
		-- change to "Gem" instead of displaying gem stats
		if itemType == "Gem" and itemSubType ~= "Artifact\nRelic" then
			itemSubType = "Gem"
		end
		-- change to "1H" to fit frame
		if string.find(itemSubType, "One%-Handed") then
			itemSubType, _ = string.gsub(itemSubType, "One%-Handed", "1H")
		end
		-- change to "2H" to fit frame
		if string.find(itemSubType, "Two%-Handed") then
			itemSubType, _ = string.gsub(itemSubType, "Two%-Handed", "2H")
		end
		-- change to "Weapon" to fit frame, instead of super long weapon type
		if itemType ~= "Armor" and itemEquipLoc:len() > 0 then
			itemEquipLoc = "Inv_Weapon"
		end
		-- if cloak, do not list as cloth for clarity
		if itemEquipLoc == "INVTYPE_CLOAK" then
			itemSubType = "Misc."
		end
		-- format armor slot name and capitalize first letter, enter data
		if itemEquipLoc:len() > 0 then
			_, itemEquipLoc = strsplit("_", itemEquipLoc)
			itemEquipLoc = string.lower(itemEquipLoc)
			itemEquipLoc = itemEquipLoc:gsub("^%l", string.upper)
			entryFrame.typeEntry.text:SetText(itemSubType .. "\n" .. itemEquipLoc)
		else
			entryFrame.typeEntry.text:SetText(itemSubType)
		end
	else -- item is not cached, wait for itemLink info from server
		pendingItemLinks[ownerName .. "%" .. ownerRealm .. "%".. itemLink] = itemLink
	end
	
	-- set status timer
	local prettyTime = GetPrettyTime(statusTime)
	entryFrame.statusEntry.timeText:SetText(prettyTime)
	entryFrame.statusEntry.timer.time = statusTime
	-- update timer OnUpdate
	entryFrame.statusEntry.timer:SetScript("OnUpdate", function(self, elapsed)
		entryFrame.statusEntry.timer.timeSinceLastUpdate = entryFrame.statusEntry.timer.timeSinceLastUpdate + elapsed; 	
		if (entryFrame.statusEntry.timer.timeSinceLastUpdate > updateInterval) then
			entryFrame.statusEntry.timer.time = entryFrame.statusEntry.timer.time - round(entryFrame.statusEntry.timer.timeSinceLastUpdate, 0)
			if entryFrame.statusEntry.timer.time > 0 then
				entryFrame.statusEntry.timeText:SetText(GetPrettyTime(entryFrame.statusEntry.timer.time))
			else
				if entryFrame.statusEntry.text:GetText() == "Open" then
					CloseEntryStatus(entryFrame, 300 + tonumber(entryFrame.statusEntry.timer.time))
				else
					entryFrame.statusEntry.timer:SetScript("OnUpdate", nil)
					if InCombatLockdown() then
						table.insert(pendingDeletion, ownerName .. "%" .. ownerRealm .. "%" .. itemLink)
					else
						DeleteByItem(ownerName .. "%" .. ownerRealm .. "%" .. itemLink)
					end
				end
			end
			entryFrame.statusEntry.timer.timeSinceLastUpdate = 0;
		end
	end)
	
	-- set status with appropriate status button
	if status == "Open" then
		entryFrame.statusEntry.text:SetTextColor(0, 1, 0)
		entryFrame.statusEntry.text:SetText(status)
		if (ownerName ~= playerName) or (ownerRealm ~= playerRealm) then
			entryFrame.statusEntry.statusButtonOpen:Show()
		end
	else
		entryFrame.statusEntry.text:SetTextColor(1, 0, 0)
		entryFrame.statusEntry.text:SetText(status)
		entryFrame.statusEntry.statusButtonEnded:Show()
	end
	entryFrame.statusEntry.statusButtonOpen:SetScript('OnClick', function()
		if(entryFrame.statusEntry.rolled == "0" and (time() - tonumber(entryFrame.statusEntry.rollClicked) > 4)) then
			local rollName, _ = UnitName("player")
			local rollRealm = GetRealmName()
			local _, _, rollClass = UnitClass("player")
			C_ChatInfo.SendAddonMessage(addonPrefix, "RR%"..rollName.."%"..rollRealm.."%"..rollClass.."%"..ownerName.."%"..ownerRealm.."%"..itemLink, "RAID")
			entryFrame.statusEntry.rollClicked = time()
		elseif (entryFrame.statusEntry.rolled == "0" and (time() - tonumber(entryFrame.statusEntry.rollClicked) < 4)) then
			print("STW: You rolled on this item recently, please wait a few seconds before trying again.")
		else
			print("STW: You already rolled on this item!")
		end
	end)
end

-- entry frame creation
-- returns clean entryFrame
local function CreateEntry()
	local numEntries = table.getn(entries)
	local scrollContent = menuFrame.scrollFrame.scrollContent
	
	-- entry frame
	local entryFrame = AcquireFrame(scrollContent)
	entryFrame:Show()
	entryFrame:SetHeight(entryHeight)
	entryFrame:SetWidth(scrollContent:GetWidth())
	-- align to top-most existing entry, or scrollContent if none
	if numEntries == 0 then
		entryFrame:SetPoint("TOPLEFT")
	else
		entryFrame:SetPoint("TOPLEFT", entries[numEntries], "BOTTOMLEFT")
	end
	
	-- owner entry
	if not entryFrame.ownerEntry then
		entryFrame.ownerEntry = CreateFrame("Frame", nil, entryFrame)
	end
	entryFrame.ownerEntry:EnableMouse()
	entryFrame.ownerEntry:SetPoint("LEFT")
	entryFrame.ownerEntry:SetHeight(entryFrame:GetHeight())
	entryFrame.ownerEntry:SetWidth(140)
	if not entryFrame.ownerEntry:GetBackdrop() then
		entryFrame.ownerEntry:SetBackdrop({edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 12})
	end
	
	-- item entry
	if not entryFrame.itemEntry then
		entryFrame.itemEntry = CreateFrame("Frame", nil, entryFrame)
	end
	entryFrame.itemEntry:EnableMouse()
	entryFrame.itemEntry:RegisterEvent("MODIFIER_STATE_CHANGED")
	entryFrame.itemEntry:SetPoint("LEFT", entryFrame.ownerEntry, "RIGHT")
	entryFrame.itemEntry:SetHeight(entryFrame:GetHeight())
	entryFrame.itemEntry:SetWidth(310)
	if not entryFrame.itemEntry:GetBackdrop() then
		entryFrame.itemEntry:SetBackdrop({edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 12})
	end
	
	-- type entry
	if not entryFrame.typeEntry then
		entryFrame.typeEntry = CreateFrame("Frame", nil, entryFrame)
	end
	entryFrame.typeEntry:SetPoint("LEFT", entryFrame.itemEntry, "RIGHT")
	entryFrame.typeEntry:SetHeight(entryFrame:GetHeight())
	entryFrame.typeEntry:SetWidth(70)
	if not entryFrame.typeEntry:GetBackdrop() then
		entryFrame.typeEntry:SetBackdrop({edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 12})
	end
	
	-- ilevel entry
	if not entryFrame.ilvlEntry then
		entryFrame.ilvlEntry = CreateFrame("Frame", nil, entryFrame)
	end
	entryFrame.ilvlEntry:SetPoint("LEFT", entryFrame.typeEntry, "RIGHT")
	entryFrame.ilvlEntry:SetHeight(entryFrame:GetHeight())
	entryFrame.ilvlEntry:SetWidth(50)
	if not entryFrame.ilvlEntry:GetBackdrop() then
		entryFrame.ilvlEntry:SetBackdrop({edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 12})
	end
	
	-- winning entry
	if not entryFrame.winningEntry then
		entryFrame.winningEntry = CreateFrame("Frame", nil, entryFrame)
	end
	entryFrame.winningEntry:SetPoint("LEFT", entryFrame.ilvlEntry, "RIGHT")
	entryFrame.winningEntry:SetHeight(entryFrame:GetHeight())
	entryFrame.winningEntry:SetWidth(140)
	entryFrame.winningEntry.roll = 0
	entryFrame.winningEntry.name = ""
	entryFrame.winningEntry.realm = ""
	entryFrame.winningEntry.itemLink = ""
	if not entryFrame.winningEntry:GetBackdrop() then
		entryFrame.winningEntry:SetBackdrop({edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 12})
	end
	
	-- status entry
	if not entryFrame.statusEntry then
		entryFrame.statusEntry = CreateFrame("Frame", nil, entryFrame)
	end
	entryFrame.statusEntry:SetPoint("LEFT", entryFrame.winningEntry, "RIGHT")
	entryFrame.statusEntry:SetHeight(entryFrame:GetHeight())
	entryFrame.statusEntry:SetWidth(55)
	entryFrame.statusEntry.status = "Open"
	entryFrame.statusEntry.rolled = "0"
	entryFrame.statusEntry.rollClicked = "0"
	if not entryFrame.statusEntry:GetBackdrop() then
		entryFrame.statusEntry:SetBackdrop({edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 12})
	end
	
	-- owner info
	if not entryFrame.ownerEntry.text then
		entryFrame.ownerEntry.text = entryFrame.ownerEntry:CreateFontString(nil,"OVERLAY", "GameFontNormal")
	end
	entryFrame.ownerEntry.text:SetFont("Fonts\\FRIZQT__.TTF", 12)
	entryFrame.ownerEntry.text:SetPoint("CENTER")
	entryFrame.ownerEntry.text:SetTextColor(1, 1, 1)
	entryFrame.ownerEntry.ownerName = ""
	entryFrame.ownerEntry.ownerRealm = ""
	entryFrame.ownerEntry.text:SetText("")
	
	-- target owner button
	if InCombatLockdown() then
		table.insert(pendingButtonCreations, entryFrame.ownerEntry)
	else
		if not entryFrame.ownerEntry.targetButton then
			entryFrame.ownerEntry.targetButton = AcquireButton(entryFrame.ownerEntry)
			entryFrame.ownerEntry.targetButton:Show()
		end
		entryFrame.ownerEntry.targetButton:SetSize(140, 50)
		entryFrame.ownerEntry.targetButton:SetPoint("CENTER")
		entryFrame.ownerEntry.targetButton:SetAttribute("type1", "macro")
		entryFrame.ownerEntry.targetButton:SetAttribute("macrotext", "/targetexact ")
		-- owner entry mouseover
		entryFrame.ownerEntry.targetButton:SetScript('OnEnter', function()
			-- add frame highlight
			entryFrame.ownerEntry:SetBackdrop({edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 16})
			entryFrame.ownerEntry:SetBackdropBorderColor(1, 1, 0)
		end)
		entryFrame.ownerEntry.targetButton:SetScript('OnLeave', function()
			-- remove frame highlight
			entryFrame.ownerEntry:SetBackdrop({edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 12})
			entryFrame.ownerEntry:SetBackdropBorderColor(1, 1, 1)
		end)
		entryFrame.ownerEntry.targetButton:SetScript('OnMouseUp', function()
		end)
	end
	
	--  'hide' button to remove entries
	if not entryFrame.ownerEntry.hideButton then
		entryFrame.ownerEntry.hideButton = CreateFrame("Button", nil, entryFrame.ownerEntry, "HideButton")
	end
	entryFrame.ownerEntry.hideButton:SetPoint("TOPLEFT")
	entryFrame.ownerEntry.hideButton:SetScript('OnClick', function()
	end)
	entryFrame.ownerEntry.hideButton:Hide()
	--  'end' button to end listings
	if not entryFrame.ownerEntry.endButton then
		entryFrame.ownerEntry.endButton = CreateFrame("Button", nil, entryFrame.ownerEntry, "EndButton")
	end
	entryFrame.ownerEntry.endButton:SetPoint("TOPLEFT", -3, 0)
	entryFrame.ownerEntry.endButton:SetScript('OnClick', function()
	end)
	entryFrame.ownerEntry.endButton:Hide()
	
	-- item info
	if not entryFrame.itemEntry.text then
		entryFrame.itemEntry.text = entryFrame.itemEntry:CreateFontString(nil,"OVERLAY", "GameFontNormal")
	end
	entryFrame.itemEntry.text:SetFont("Fonts\\FRIZQT__.TTF", 12)
	entryFrame.itemEntry.text:SetPoint("CENTER")
	entryFrame.itemEntry.text:SetTextColor(1, 1, 1)
	entryFrame.itemEntry.text:SetText("")
	entryFrame.itemEntry.itemLink = ""
	-- item link mouseover
	entryFrame.itemEntry:SetScript('OnEnter', function()
	end)
	entryFrame.itemEntry:SetScript('OnLeave', function()
		-- remove frame highlight
		entryFrame.itemEntry:SetBackdrop({edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 12})
		entryFrame.itemEntry:SetBackdropBorderColor(1, 1, 1)
		ItemTooltip1:Hide()
		ItemTooltip1:SetOwner(WorldFrame, "ANCHOR_NONE")
		ItemTooltip2:Hide() 
		ItemTooltip2:SetOwner(WorldFrame, "ANCHOR_NONE")
		ItemTooltip3:Hide() 
		ItemTooltip3:SetOwner(WorldFrame, "ANCHOR_NONE")
	end)
	-- item link add to chat box
	entryFrame.itemEntry:SetScript('OnMouseUp', function()
	end)
	
	-- type info
	if not entryFrame.typeEntry.text then
		entryFrame.typeEntry.text = entryFrame.typeEntry:CreateFontString(nil,"OVERLAY", "GameFontNormal")
	end
	entryFrame.typeEntry.text:SetFont("Fonts\\FRIZQT__.TTF", 12)
	entryFrame.typeEntry.text:SetPoint("CENTER")
	entryFrame.typeEntry.text:SetTextColor(1,1,1)
	entryFrame.typeEntry.text:SetText("")
	
	-- ilevel info
	if not entryFrame.ilvlEntry.text then
		entryFrame.ilvlEntry.text = entryFrame.ilvlEntry:CreateFontString(nil,"OVERLAY", "GameFontNormal")
	end
	entryFrame.ilvlEntry.text:SetFont("Fonts\\FRIZQT__.TTF", 12)
	entryFrame.ilvlEntry.text:SetPoint("CENTER")
	entryFrame.ilvlEntry.text:SetTextColor(1,1,1)
	entryFrame.ilvlEntry.text:SetText("")
	
	-- winning roll info
	if not entryFrame.winningEntry.text then
		entryFrame.winningEntry.text = entryFrame.winningEntry:CreateFontString(nil,"OVERLAY", "GameFontNormal")
	end
	entryFrame.winningEntry.text:SetFont("Fonts\\FRIZQT__.TTF", 12)
	entryFrame.winningEntry.text:SetPoint("CENTER")
	entryFrame.winningEntry.text:SetTextColor(.6,.6,.6)
	entryFrame.winningEntry.text:SetText("None")
	entryFrame.winningEntry.rollNames = {}
	
	-- winning roll target button
	if InCombatLockdown() then
		table.insert(pendingButtonCreations, entryFrame.winningEntry)
	else
		if not entryFrame.winningEntry.targetButton then
			entryFrame.winningEntry.targetButton = AcquireButton(entryFrame.winningEntry)
			entryFrame.winningEntry.targetButton:Show()
		end
		entryFrame.winningEntry.targetButton:SetSize(140, 50)
		entryFrame.winningEntry.targetButton:SetPoint("CENTER")
		entryFrame.winningEntry.targetButton:SetAttribute("type1", "macro")
		entryFrame.winningEntry.targetButton:SetAttribute("macrotext", "/targetexact ")
		entryFrame.winningEntry.targetButton:SetScript('OnMouseUp', function()
		end)
		-- winning entry mouseover
		entryFrame.winningEntry.targetButton:SetScript('OnEnter', function()
			-- add frame highlight
			entryFrame.winningEntry:SetBackdrop({edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 16})
			entryFrame.winningEntry:SetBackdropBorderColor(1, 1, 0)
		end)
		entryFrame.winningEntry.targetButton:SetScript('OnLeave', function()
			-- remove frame highlight
			entryFrame.winningEntry:SetBackdrop({edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 12})
			entryFrame.winningEntry:SetBackdropBorderColor(1, 1, 1)
		end)
	end

	-- status time text
	if not entryFrame.statusEntry.timeText then
		entryFrame.statusEntry.timeText = entryFrame.statusEntry:CreateFontString(nil,"OVERLAY", "GameFontNormal")
	end
	entryFrame.statusEntry.timeText:SetFont("Fonts\\FRIZQT__.TTF", 12)
	entryFrame.statusEntry.timeText:SetPoint("CENTER", 0, 5)
	entryFrame.statusEntry.timeText:SetTextColor(1,1,1)
	entryFrame.statusEntry.timeText:SetText("")
	
	--status timer frame
	if not entryFrame.statusEntry.timer then
		entryFrame.statusEntry.timer = CreateFrame("Frame", nil, nil)
	end
	entryFrame.statusEntry.timer.time = 0 -- for future reference
	entryFrame.statusEntry.timer.timeSinceLastUpdate = 0 -- for future reference
	entryFrame.statusEntry.timer:SetScript('OnUpdate', function()
	end)
	
	-- status info
	if not entryFrame.statusEntry.text then
		entryFrame.statusEntry.text = entryFrame.statusEntry:CreateFontString(nil,"OVERLAY", "GameFontNormal")
	end
	entryFrame.statusEntry.text:SetFont("Fonts\\FRIZQT__.TTF", 12)
	entryFrame.statusEntry.text:SetPoint("BOTTOM", entryFrame.statusEntry.timeText,"TOP")
	entryFrame.statusEntry.text:SetTextColor(1,1,1)
	entryFrame.statusEntry.text:SetText("00:00")
	
	-- status roll button
	if not entryFrame.statusEntry.statusButtonOpen then
		entryFrame.statusEntry.statusButtonOpen = CreateFrame("Button", nil, entryFrame.statusEntry, "RollButton")
	end
	entryFrame.statusEntry.statusButtonOpen:SetPoint("TOP", entryFrame.statusEntry.timeText, "BOTTOM", 0, 0)
	entryFrame.statusEntry.statusButtonOpen:SetSize(24, 24)
	entryFrame.statusEntry.statusButtonOpen:Hide()
	
	-- status X button (no roll)
	if not entryFrame.statusEntry.statusButtonEnded then
		entryFrame.statusEntry.statusButtonEnded = CreateFrame("Button", nil, entryFrame.statusEntry, "NoRollButton")
	end
	entryFrame.statusEntry.statusButtonEnded:SetPoint("TOP", entryFrame.statusEntry.timeText, "BOTTOM", 0, 0)
	entryFrame.statusEntry.statusButtonEnded:SetSize(24, 24)
	entryFrame.statusEntry.statusButtonEnded:Hide()
	table.insert(entries, entryFrame)
	
	-- adjust scrollbar
	numEntries = table.getn(entries)
	if numEntries*entryHeight > menuFrame.scrollFrame:GetHeight() then
		menuFrame.scrollBar.max = menuFrame.scrollBar.max + entryHeight
		menuFrame.scrollBar:SetMinMaxValues(0, menuFrame.scrollBar.max)
		menuFrame.scrollFrame:SetScript("OnMouseWheel", function(self, delta) 
			local curValue = menuFrame.scrollBar:GetValue()
		
			if IsShiftKeyDown() and (delta > 0) then
				menuFrame.scrollBar:SetValue(0)
			elseif IsShiftKeyDown() and (delta < 0) then
				menuFrame.scrollBar:SetValue(menuFrame.scrollBar.max)
			elseif (delta < 0) and (curValue < menuFrame.scrollBar.max) then
				menuFrame.scrollBar:SetValue(curValue + 20)
			elseif (delta > 0) and (curValue > 1) then
				menuFrame.scrollBar:SetValue(curValue - 20)
			end
		end)
	end
	
	return entryFrame
end

-- setup initial STW frame
local function InitMain()
	-- main frame
	menuFrame:SetWidth(windowWidth)
	menuFrame:SetHeight(windowHeight)
	menuFrame:SetPoint("CENTER", 0, 0)	
	menuFrame:Hide()
	
	-- title frame
	local titleFrame = CreateFrame("Frame", nil, menuFrame, "TitleFrame")
	titleFrame:SetHeight(20)
	titleFrame:SetWidth(160)
	titleFrame:SetPoint("BOTTOM", menuFrame, "TOP")
	menuFrame.titleFrame = titleFrame
	
	-- title texture
	local titleTexture = titleFrame:CreateTexture(nil,"ARTWORK")
	titleTexture:SetTexture("Interface/DialogFrame/UI-DialogBox-Gold-Header")
	titleTexture:SetWidth(titleFrame:GetWidth()*2)
	titleTexture:SetPoint("TOP", titleFrame, 0, 10)
	
	-- title string
	local titleStr = titleFrame:CreateFontString(nil,"OVERLAY", "GameFontNormal")
	titleStr:SetFont("Fonts\\FRIZQT__.TTF", 12, "OUTLINE")
	titleStr:SetPoint("CENTER", titleFrame)
	titleStr:SetTextColor(.9,.9,0,1)
	titleStr:SetText("Share the Wealth")
	
	-- x button
	local xButton = CreateFrame("Button", nil, menuFrame, "UIPanelCloseButton")
	xButton:SetPoint("TOPRIGHT")
	xButton:SetSize(32, 32)
	menuFrame.xButton = xButton
	
	-- scroll frame
	local scrollFrame = CreateFrame("ScrollFrame", nil, menuFrame)
	scrollFrame:EnableMouseWheel(true)
	scrollFrame:SetHeight(menuFrame:GetHeight()-74)
	scrollFrame:SetPoint("TOPRIGHT", menuFrame, "TOPRIGHT", -7, -28)
	scrollFrame:SetPoint("TOPLEFT", menuFrame, 7, -28)
	scrollFrame:SetClipsChildren(true)
	menuFrame.scrollFrame = scrollFrame
	
	-- scroll bar
	local scrollBar = CreateFrame("Slider", nil, scrollFrame, "UIPanelScrollBarTemplate")
	scrollBar:SetPoint("TOPRIGHT", -1, -16)
	scrollBar:SetPoint("BOTTOMRIGHT", -1, 16)
	scrollBar.max = 0
	scrollBar:SetMinMaxValues(0, 0)
	scrollBar:SetValueStep(1)
	scrollBar.scrollStep = 1
	scrollBar:SetValue(0)
	scrollBar:SetWidth(16)
	scrollBar:SetScript("OnValueChanged", function(self, value) self:GetParent():SetVerticalScroll(value) end)
	menuFrame.scrollBar = scrollBar
	
	-- scroll content
	local scrollContent = CreateFrame("Frame", nil, scrollFrame)
	scrollContent:SetSize(scrollFrame:GetWidth(), scrollFrame:GetHeight())
	scrollFrame.scrollContent = scrollContent
	scrollFrame:SetScrollChild(scrollContent)
	
	-- mousewheel scrolling
	scrollFrame:SetScript("OnMouseWheel", function(self, delta) 
		local curValue = menuFrame.scrollBar:GetValue()
	
		if IsShiftKeyDown() and (delta > 0) then
			menuFrame.scrollBar:SetValue(0)
		elseif IsShiftKeyDown() and (delta < 0) then
			menuFrame.scrollBar:SetValue(scrollBar.max)
		elseif (delta < 0) and (curValue < scrollBar.max) then
			menuFrame.scrollBar:SetValue(curValue + 20)
		elseif (delta > 0) and (curValue > 1) then
			menuFrame.scrollBar:SetValue(curValue - 20)
		end
	end)
	
	-- bottom frame
	local bottomFrame = CreateFrame("Frame", nil, menuFrame, "BottomFrameTemplate")
	bottomFrame:SetHeight(40)
	bottomFrame:SetWidth(menuFrame:GetWidth())
	bottomFrame:SetPoint("BOTTOMLEFT", menuFrame, 7, 5)
	bottomFrame:SetPoint("BOTTOMRIGHT", menuFrame, -7, 5)
	menuFrame.bottomFrame = bottomFrame
	
	-- quote string
	if STW_Config["HideQuote"] == "false" then
		local quoteStr = bottomFrame:CreateFontString(nil,"OVERLAY", "GameFontNormal")
		quoteStr:SetFont("Fonts\\FRIZQT__.TTF", 12)
		quoteStr:SetPoint("CENTER")
		quoteStr:SetTextColor(1,1,1,1)
		quoteStr:SetText(quotes[math.random(table.getn(quotes))])
	end
	
	-- add-item slot
	local slotFrame = CreateFrame("Frame", nil, bottomFrame, "ItemSlotTemplate")
	slotFrame:SetHeight(58)
	slotFrame:SetWidth(58)
	slotFrame:SetPoint("RIGHT", bottomFrame, 10, 0)
	slotFrame.itemLink = "" -- for future reference
	slotFrame.itemTexture = slotFrame:CreateTexture(nil, "ARTWORK")
	bottomFrame.slotFrame = slotFrame
	
	slotFrame:SetScript("OnMouseUp", function(self,button)
		local infoType, itemID, itemLink = GetCursorInfo()
		if infoType == "item" then
			local itemIcon = GetItemIcon(itemID)
			slotFrame.itemLink = itemLink
			slotFrame.itemTexture:SetTexture(itemIcon)
			slotFrame.itemTexture:SetWidth(35)
			slotFrame.itemTexture:SetHeight(35)
			slotFrame.itemTexture:SetPoint("CENTER")
			ClearCursor()
			ItemTooltip1:SetOwner(WorldFrame, "ANCHOR_NONE")
			ItemTooltip1:ClearLines()
			ItemTooltip1:SetHyperlink(itemLink)
			ItemTooltip1:SetPoint("BOTTOMLEFT", slotFrame, "BOTTOMRIGHT")
			ItemTooltip1:Show()
			-- item link mouseover
			slotFrame:SetScript('OnEnter', function()
				slotFrame:SetBackdrop({bgFile="Interface/Buttons/UI-EmptySlot-White"})
				ItemTooltip1:ClearLines()
				ItemTooltip1:SetHyperlink(itemLink)
				ItemTooltip1:SetPoint("BOTTOMLEFT", slotFrame, "BOTTOMRIGHT")
				ItemTooltip1:Show()
			end)
			slotFrame:SetScript('OnLeave', function()
				slotFrame:SetBackdrop({bgFile="Interface/Buttons/UI-EmptySlot-Disabled"})
				ItemTooltip1:Hide()
				ItemTooltip1:SetOwner(WorldFrame, "ANCHOR_NONE")
			end)
		end
	end)
	
	
	-- add item string
	local addItemStr = slotFrame:CreateFontString(nil,"OVERLAY", "GameFontNormal")
	addItemStr:SetFont("Fonts\\FRIZQT__.TTF", 12, "OUTLINE")
	addItemStr:SetPoint("RIGHT", slotFrame, "LEFT", -7, 0)
	addItemStr:SetTextColor(1,1,1,1)
	addItemStr:SetText("Add\nItem")
	
	-- add-item confirm button
	local slotConfirmButton = CreateFrame("Button", nil, bottomFrame, "AddButton")
	slotConfirmButton:SetSize(20.5, 20.4)
	slotConfirmButton:SetPoint("RIGHT", slotFrame, "LEFT", 11, 10)
	
	slotConfirmButton:SetScript("OnMouseUp", function(self,button)
		if slotFrame.itemLink ~= "" then
			local dialog = StaticPopup_Show("STW_CONFIRM_ADD")
			local ownerName, _ = UnitName("player")
			local ownerRealm = GetRealmName()
			local itemLink = slotFrame.itemLink
			if (dialog) then
				dialog.data = ownerName .. "%" .. ownerRealm .. "%" .. itemLink
			end
		end
	end)
	
	-- add-item cancel button
	local slotCancelButton = CreateFrame("Button", nil, bottomFrame, "EndButton")
	slotCancelButton:SetSize(32, 32)
	slotCancelButton:SetPoint("RIGHT", slotFrame, "LEFT", 15, -12)
	
	slotCancelButton:SetScript("OnMouseUp", function(self,button)
		if slotFrame.itemLink ~= "" then 
			slotFrame.itemLink = ""
			slotFrame.itemTexture:SetTexture("")
			slotFrame.itemTexture:SetWidth(35)
			slotFrame.itemTexture:SetHeight(35)
			slotFrame.itemTexture:SetPoint("CENTER")
			PlaySound(838, "master")
			slotFrame:SetScript('OnEnter', function()
				slotFrame:SetBackdrop({bgFile="Interface/Buttons/UI-EmptySlot-White"})
				ItemTooltip1:ClearLines()
				ItemTooltip1:Show()
			end)
		end
	end)
	
	-- info button
	local infoButton = CreateFrame("Button", nil, bottomFrame, "InfoButton")
	infoButton:SetPoint("LEFT", 5, 10)
	infoButton:SetScript('OnEnter', function()
		ItemTooltip1:ClearLines()
		ItemTooltip1:AddLine("Share the Wealth -- Help Info", .5, 1, 0)
		ItemTooltip1:AddLine("Command Line:")
		ItemTooltip1:AddDoubleLine("   Show/hide:", "/stw", 1, .8, 0, 1,1,1)
		ItemTooltip1:AddDoubleLine("   Add item:", "/stw [add,a] [item]", 1, .8, 0, 1,1,1)
		ItemTooltip1:AddDoubleLine("   Reset position:", "/stw reset",1, .8, 0, 1,1,1)
		ItemTooltip1:AddDoubleLine("   Command help:", "/stw help",1, .8, 0, 1,1,1)
		ItemTooltip1:AddLine(" ")
		ItemTooltip1:AddLine("GUI:")
		ItemTooltip1:AddDoubleLine("   Add item:", "Lower-right \"add item\" box", 1, .8, 0, 1,1,1)
		ItemTooltip1:AddDoubleLine("   End item:", "Click \"X\" in owner field", 1, .8, 0, 1,1,1)
		ItemTooltip1:AddDoubleLine("   Hide item:", "Click \"-\" in owner field", 1, .8, 0, 1,1,1)
		ItemTooltip1:AddDoubleLine("   Roll on item:", "Click dice in status field", 1, .8, 0, 1,1,1)
		ItemTooltip1:AddDoubleLine("   Target player:", "Left-click player field", 1, .8, 0, 1,1,1)
		ItemTooltip1:AddDoubleLine("   Trade player:", "Right-click player field", 1, .8, 0, 1,1,1)
		ItemTooltip1:AddDoubleLine("   Tradewindow item:", "Right-click player field again", 1, .8, 0, 1,1,1)
		ItemTooltip1:SetPoint("BOTTOMRIGHT", menuFrame, "BOTTOMLEFT")
		ItemTooltip1:Show()
	end)
	infoButton:SetScript('OnLeave', function()
		ItemTooltip1:Hide()
		ItemTooltip1:SetOwner(WorldFrame, "ANCHOR_NONE")
	end)
	
	-- Settings button
	local settingsButton = CreateFrame("Button", nil, bottomFrame, "SettingsButton")
	settingsButton:SetPoint("TOP", infoButton, "BOTTOM")
	settingsButton:SetScript('OnClick', function()
		InterfaceOptionsFrame_OpenToCategory(STW_ConfigFrame.name)
		InterfaceOptionsFrame_OpenToCategory(STW_ConfigFrame.name)
	end)
	
	-- column headers
	-- header frame
	local headerFrame = CreateFrame("Frame", nil, menuFrame)
	headerFrame:SetPoint("BOTTOMLEFT", scrollFrame, "TOPLEFT")
	headerFrame:SetWidth(windowWidth-12)
	headerFrame:SetHeight(20)
	menuFrame.headerFrame = headerFrame
	
	local headerTexture = headerFrame:CreateTexture(nil, "BACKGROUND")
	headerTexture:SetTexture("Interface/DialogFrame/UI-DialogBox-Gold-Background")
	headerTexture:SetWidth(headerFrame:GetWidth())
	headerTexture:SetHeight(headerFrame:GetHeight())
	headerTexture:SetAllPoints()
	
	-- owner header
	local ownerHeader = CreateFrame("Frame", nil, headerFrame)
	ownerHeader:SetPoint("LEFT")
	ownerHeader:SetHeight(headerFrame:GetHeight())
	ownerHeader:SetWidth(140)
	ownerHeader:SetBackdrop({edgeFile="Interface\\GLUES\\COMMON\\TextPanel-Border", edgeSize=12})	
	headerFrame.ownerHeader = ownerHeader
	
	local ownerStr = ownerHeader:CreateFontString(nil,"OVERLAY", "GameFontNormal")
	ownerStr:SetFont("Fonts\\FRIZQT__.TTF", 12)
	ownerStr:SetPoint("CENTER")
	ownerStr:SetTextColor(1,1,1,1)
	ownerStr:SetText("Owner")
	
	-- item header
	local itemHeader = CreateFrame("Frame", nil, headerFrame)
	itemHeader:SetPoint("LEFT", ownerHeader, "RIGHT")
	itemHeader:SetHeight(headerFrame:GetHeight())
	itemHeader:SetWidth(310)
	itemHeader:SetBackdrop({edgeFile="Interface\\GLUES\\COMMON\\TextPanel-Border", edgeSize=12})	
	headerFrame.itemHeader = itemHeader
	
	local itemStr = itemHeader:CreateFontString(nil,"OVERLAY", "GameFontNormal")
	itemStr:SetFont("Fonts\\FRIZQT__.TTF", 12)
	itemStr:SetPoint("CENTER")
	itemStr:SetTextColor(1,1,1,1)
	itemStr:SetText("Item Name")
	
	-- type header
	local typeHeader = CreateFrame("Frame", nil, headerFrame)
	typeHeader:SetPoint("LEFT", itemHeader, "RIGHT")
	typeHeader:SetHeight(headerFrame:GetHeight())
	typeHeader:SetWidth(70)
	typeHeader:SetBackdrop({edgeFile="Interface\\GLUES\\COMMON\\TextPanel-Border", edgeSize=12})	
	headerFrame.typeHeader = typeHeader
	
	local typeStr = typeHeader:CreateFontString(nil,"OVERLAY", "GameFontNormal")
	typeStr:SetFont("Fonts\\FRIZQT__.TTF", 12)
	typeStr:SetPoint("CENTER")
	typeStr:SetTextColor(1,1,1,1)
	typeStr:SetText("Type")
	
	-- ilvl header
	local ilvlHeader = CreateFrame("Frame", nil, headerFrame)
	ilvlHeader:SetPoint("LEFT", typeHeader, "RIGHT")
	ilvlHeader:SetHeight(headerFrame:GetHeight())
	ilvlHeader:SetWidth(50)
	ilvlHeader:SetBackdrop({edgeFile="Interface\\GLUES\\COMMON\\TextPanel-Border", edgeSize=12})	
	headerFrame.ilvlHeader = ilvlHeader
	
	local ilvlStr = ilvlHeader:CreateFontString(nil,"OVERLAY", "GameFontNormal")
	ilvlStr:SetFont("Fonts\\FRIZQT__.TTF", 12)
	ilvlStr:SetPoint("CENTER")
	ilvlStr:SetTextColor(1,1,1,1)
	ilvlStr:SetText("iLevel")
	
	-- winningHeader
	local winningHeader = CreateFrame("Frame", nil, headerFrame)
	winningHeader:SetPoint("LEFT", ilvlHeader, "RIGHT")
	winningHeader:SetHeight(headerFrame:GetHeight())
	winningHeader:SetWidth(140)
	winningHeader:SetBackdrop({edgeFile="Interface\\GLUES\\COMMON\\TextPanel-Border", edgeSize=12})	
	headerFrame.winningHeader = winningHeader
	
	local winningStr = winningHeader:CreateFontString(nil,"OVERLAY", "GameFontNormal")
	winningStr:SetFont("Fonts\\FRIZQT__.TTF", 12)
	winningStr:SetPoint("CENTER")
	winningStr:SetTextColor(1,1,1,1)
	winningStr:SetText("Top Roll")
	
	-- statusHeader
	local statusHeader = CreateFrame("Frame", nil, headerFrame)
	statusHeader:SetPoint("LEFT", winningHeader, "RIGHT")
	statusHeader:SetHeight(headerFrame:GetHeight())
	statusHeader:SetWidth(55)
	statusHeader:SetBackdrop({edgeFile="Interface\\GLUES\\COMMON\\TextPanel-Border", edgeSize=12})	
	headerFrame.statusHeader = statusHeader
	
	local statusStr = statusHeader:CreateFontString(nil,"OVERLAY", "GameFontNormal")
	statusStr:SetFont("Fonts\\FRIZQT__.TTF", 12)
	statusStr:SetPoint("CENTER")
	statusStr:SetTextColor(1,1,1,1)
	statusStr:SetText("Status")
	
	-- dialog boxes
	StaticPopupDialogs["STW_CONFIRM_HIDE"] = {
	text = "Hide item listing? This will not affect other players, and cannot be undone.",
	button1 = "Yes",
	button2 = "No",
	OnAccept = function(self, data)
		if InCombatLockdown() then
			print("STW: Error, currently in combat.")
			return
		end	
		DeleteByItem(data)
	end,
	timeout = 10,
	whileDead = true,
	hideOnEscape = true,
	preferredIndex = 3
	}
	
	StaticPopupDialogs["STW_CONFIRM_END"] = {
	text = "End item listing for all players? This cannot be undone.",
	button1 = "Yes",
	button2 = "No",
	OnAccept = function(self, data)
		if InCombatLockdown() then
			print("STW: Error, currently in combat.")
			return
		end
		index = FindEntry(data)
		if index > 0 then
			CloseEntryStatus(entries[index], 300)
			C_ChatInfo.SendAddonMessage(addonPrefix, "D%"..data, "RAID")
		end
	end,
	timeout = 10,
	whileDead = true,
	hideOnEscape = true,
	preferredIndex = 3
	}
	
	StaticPopupDialogs["STW_CONFIRM_ADD"] = {
	text = "Add item listing?",
	button1 = "Yes",
	button2 = "No",
	OnAccept = function(self, data)
		if not (UnitInParty("player") or UnitInRaid("player")) then
			print("STW: Error, no group detected.")
			return
		end
		local index = FindEntry(data)
		if index > 0 then
			print("STW: Duplicate entry found. Please wait for your listing to complete before adding it again.")
			PlaySound(846, "master")
			menuFrame.bottomFrame.slotFrame.itemTexture:SetTexture("")
			menuFrame.bottomFrame.slotFrame.itemTexture:SetWidth(35)
			menuFrame.bottomFrame.slotFrame.itemTexture:SetHeight(35)
			menuFrame.bottomFrame.slotFrame.itemTexture:SetPoint("CENTER")
			menuFrame.bottomFrame.slotFrame.itemLink = ""
			slotFrame:SetScript('OnEnter', function()
				slotFrame:SetBackdrop({bgFile="Interface/Buttons/UI-EmptySlot-White"})
				ItemTooltip1:ClearLines()
				ItemTooltip1:Show()
			end)
			return
		end
		local ownerName, ownerRealm, itemLink = strsplit("%", data)
		local canTrade, _, _ = IsTradeable(itemLink)
		if canTrade then
			local newEntry = CreateEntry()
			local _, _, ownerClass = UnitClass("player")
			C_ChatInfo.SendAddonMessage(addonPrefix, "A%"..ownerName.."%"..ownerRealm.."%"..ownerClass.."%"..itemLink, "RAID")
			UpdateEntry(newEntry, ownerName, ownerRealm, ownerClass, itemLink, "Open", 180)
			menuFrame.bottomFrame.slotFrame.itemTexture:SetTexture("")
			menuFrame.bottomFrame.slotFrame.itemTexture:SetWidth(35)
			menuFrame.bottomFrame.slotFrame.itemTexture:SetHeight(35)
			menuFrame.bottomFrame.slotFrame.itemTexture:SetPoint("CENTER")
			menuFrame.bottomFrame.slotFrame.itemLink = ""
			PlaySound(891, "master")
			if UnitInRaid("player") then
				SendChatMessage("STW: "..itemLink.." listed for rolls!", "RAID")
			elseif UnitInParty("player") then
				SendChatMessage("STW: "..itemLink.." listed for rolls!", "PARTY")
			end
			slotFrame:SetScript('OnEnter', function()
				slotFrame:SetBackdrop({bgFile="Interface/Buttons/UI-EmptySlot-White"})
				ItemTooltip1:ClearLines()
				ItemTooltip1:Show()
			end)
		else	
			print("STW: Item " .. itemLink .. " cannot be traded, or was not found in bags.")
			UIErrorsFrame:AddMessage("Item cannot be traded!", 1.0, 0.0, 0.0, 53, 5)
			PlaySoundFile("Sound/character/Human/FemaleErrorMessages/HumanFemale_err_canttradesoulbounditem01.ogg", "master")
		end	
	end,
	timeout = 10,
	whileDead = true,
	hideOnEscape = true,
	preferredIndex = 3
	}
	
	-- Register window sound
	menuFrame:SetScript("OnShow", function(self)
		PlaySoundFile("Sound/Interface/uCharacterSheetOpen.ogg")
	end)
	menuFrame:SetScript("OnHide", function(self)
		PlaySoundFile("Sound/Interface/uCharacterSheetClose.ogg")
	end)
	
	----- Saved vars config -----
	-- Register for escape close
	if STW_Config["MenuEscape"] == "true" then
		tinsert(UISpecialFrames, menuFrame:GetName())
	end
	-- Disable loot noise
	if STW_Config["DisableLootNoise"] == "true" then
		STW_MinimapAnimations:SetScript("OnLoop", nil)
	end
	-- Free minimap button
	if STW_Config["FreeMinimap"] == "true" then
		Minimap_Slider1_Editbox:Disable()
		STW_MinimapButton:SetScript("OnDragStart", function()
			STW_MinimapButton:LockHighlight()
			STW_MinimapButton:StartMoving()
		end)
		STW_MinimapButton:SetScript("OnDragStop", function()
			STW_MinimapButton:UnlockHighlight()
			STW_MinimapButton:StopMovingOrSizing()
		end)
	elseif STW_Config["FreeMinimap"] == "false" then
		STW_MinimapButton:SetScript("OnDragStart", function()
			STW_MinimapButton:LockHighlight()
			STW_MinimapButton_DraggingFrame:Show()
		end)
		STW_MinimapButton:SetScript("OnDragStop", function()
			STW_MinimapButton:UnlockHighlight()
			STW_MinimapButton_DraggingFrame:Hide()
		end)
	end
	-- Lock minimap button
	if STW_Config["LockMinimap"] == "true" then
		STW_MinimapButton:RegisterForDrag()
		Minimap_Slider1_Editbox:Disable()
	end
	-- Show/hide minimap button
	if STW_Config["HideMinimap"] == "true" then
		STW_MinimapButton:Hide()
	end
end

-- Slash commands
SLASH_SHARETHEWEALTH1 = '/stw'
local function SlashHandler(msg, editbox)
	local cmd, args = msg:match("^(%S*)%s*(.-)$")
	
	-- check for combat
	if InCombatLockdown() then
		print("STW: Error, currently in combat.")
		return
	end
	
	-- handle commands
	if (cmd == "add" or cmd == "a") and args ~= "" then -- add entry
		if not (UnitInParty("player") or UnitInRaid("player")) then
			print("STW: Error, no group detected.")
			return
		end
		local ownerName, _ = UnitName("player")
		local ownerRealm = GetRealmName()
		local iterations = 0
		for itemLink in msg:gmatch("|%x+|Hitem:.-|h.-|h|r") do
			if iterations > 0 then
				break
			end
			local index = FindEntry(ownerName .. "%" .. ownerRealm .. "%" .. itemLink)
			if index > 0 then
				print("STW: Duplicate entry found. Please wait for your listing to complete before adding it again.")
				PlaySound(846, "master")
				break
			end
			local canTrade, _, _ = IsTradeable(itemLink)
			if canTrade then
				local newEntry = CreateEntry()
				local ownerName, _ = UnitName("player")
				local ownerRealm = GetRealmName()
				local _, _, ownerClass = UnitClass("player")
				C_ChatInfo.SendAddonMessage(addonPrefix, "A%"..ownerName.."%"..ownerRealm.."%"..ownerClass.."%"..itemLink, "RAID")
				UpdateEntry(newEntry, ownerName, ownerRealm, ownerClass, itemLink, "Open", 180)
				iterations = 1
				PlaySound(891, "master")
				if UnitInRaid("player") then
					SendChatMessage("STW: "..itemLink.." listed for rolls!", "RAID")
				elseif UnitInParty("player") then
					SendChatMessage("STW: "..itemLink.." listed for rolls!", "PARTY")
				end
			else	
				print("STW: Item " .. itemLink .. " cannot be traded, or was not found in bags.")
				UIErrorsFrame:AddMessage("Item cannot be traded!", 1.0, 0.0, 0.0, 53, 5)
				PlaySoundFile("Sound/character/Human/FemaleErrorMessages/HumanFemale_err_canttradesoulbounditem01.ogg", "master")
				iterations = 1
			end
		end
	elseif cmd == "show" and STW_Menu then -- show frame
		menuFrame:Show()
	elseif cmd == "hide" and STW_Menu then -- hide frame
		menuFrame:Hide()
	elseif cmd == "reset" and STW_Menu then -- reset frame position
		menuFrame:ClearAllPoints()
		menuFrame:SetPoint("CENTER", UIParent)
	elseif cmd== "config" and STW_Menu then
		InterfaceOptionsFrame_OpenToCategory(STW_ConfigFrame.name)
		InterfaceOptionsFrame_OpenToCategory(STW_ConfigFrame.name)
	elseif cmd == "" then
		if menuFrame:IsVisible() then
			menuFrame:Hide()
		else
			menuFrame:Show()
		end
	else -- error message
		print("----")
		print("Share the Wealth")
		print("Show/hide: /stw")
		print("Add item: /stw [add,a] [item]")
		print("Config: /stw config")
		print("Reset position: /stw reset")
		print("Command help: /stw help")
		print("----")
	end
end
SlashCmdList["SHARETHEWEALTH"] = SlashHandler

local eventFrame = CreateFrame("Frame")
eventFrame:SetScript("OnEvent", function(self, event, ...)
	-- chat_msg_addon: arg1, arg2, arg3, arg4 = "prefix", "message", "channel", "sender"
	local arg1, arg2, arg3, arg4 = ...
	if event == "CHAT_MSG_ADDON" and arg1 == addonPrefix and (arg3 == "RAID" or arg3 == "PARTY") then
		local cmd, cmdarg1, cmdarg2, cmdarg3, cmdarg4, cmdarg5, cmdarg6, cmdarg7 = strsplit("%", arg2)
		local playerName, _ = UnitName("player")
		local playerRealm = GetRealmName()
		-- Roll Request: (1)rollName, (2)rollRealm, (3)rollClass, (4)ownerName, (5)ownerRealm, (6)itemLink
		if(cmd == "RR" and cmdarg4 == playerName and cmdarg5 == playerRealm) then
			local index = FindEntry(playerName.."%"..playerRealm.."%"..cmdarg6)
			if index > 0 then
				local entryFrame = entries[index]
				if(entryFrame.statusEntry.status == "Open") then
					-- check if requester has already rolled
					if(entryFrame.winningEntry.rollNames[cmdarg1 .. "%" .. cmdarg2] == nil) then
						local minRoll, maxRoll
						-- this loop assigns a unique roll range to the requester, to ensure no collisions when parsing multiple rolls from a single requester on multiple items
						for i=1,30 do
							minRoll = i
							maxRoll = minRoll + 99
							if pendingRolls[cmdarg1.."%"..minRoll.."%"..maxRoll] == nil then
								break
							end
						end
						pendingRolls[cmdarg1 .. "%" .. minRoll .. "%" .. maxRoll] = cmdarg1 .. "%" .. cmdarg2 .. "%" .. cmdarg3 .. "%" .. cmdarg6
						C_ChatInfo.SendAddonMessage(addonPrefix, "RA%" .. cmdarg1 .. "%" .. cmdarg2 .. "%" .. minRoll .. "%" .. maxRoll .. "%" .. playerName .. "%" .. playerRealm .. "%" .. cmdarg6, "RAID")
						entryFrame.winningEntry.rollNames[cmdarg1 .. "%" .. cmdarg2] = "1"
					end
				end
			end
		-- Roll Ack: (1)rollName, (2)rollRealm, (3)minRoll (4)maxRoll, (5)ownerName, (6)ownerRealm, (7) itemLink
		elseif(cmd == "RA" and cmdarg1 == playerName and cmdarg2 == playerRealm) then
			local index = FindEntry(cmdarg5.."%"..cmdarg6.."%"..cmdarg7)
			if index > 0 then
				local entryFrame = entries[index]
				RandomRoll(cmdarg3, cmdarg4)
				entryFrame.statusEntry.rolled = "1"
			end
		-- Roll Update: (1)rollName, (2)rollRealm, (3)rollClass, (4)roll, (5)ownerName, (6)ownerRealm, (7) itemLink
		elseif(cmd == "U") then
			local index = FindEntry(cmdarg5.."%"..cmdarg6.."%"..cmdarg7)
			if index > 0 then
				local entryFrame = entries[index]
				local r, g, b = strsplit(":", classColors[tonumber(cmdarg3)])
				entryFrame.winningEntry.name = cmdarg1
				entryFrame.winningEntry.realm = cmdarg2
				entryFrame.winningEntry.roll = cmdarg4
				entryFrame.winningEntry.itemLink = cmdarg7
				entryFrame.winningEntry.text:SetTextColor(r, g, b)
				entryFrame.winningEntry.text:SetText(cmdarg1 .. "\n" .. cmdarg2 .. "\n" .. cmdarg4)			
				if InCombatLockdown() then
					table.insert(pendingButtonUpdates, entryFrame.winningEntry)
				else
					-- secure winning target button
					if(cmdarg2 == playerRealm) then
						entryFrame.winningEntry.targetButton:SetAttribute("macrotext", "/targetexact " .. cmdarg1)
					else
						entryFrame.winningEntry.targetButton:SetAttribute("macrotext", "/targetexact " .. cmdarg1.."-"..cmdarg2:gsub("%s+", ""))
					end
					-- trade winning
					entryFrame.winningEntry.targetButton:SetScript('OnMouseUp', function(self, button)
						if button == "RightButton" then
							if TradeFrame:IsVisible() then
								local canTrade, bag, slot = IsTradeable(cmdarg7)
								local name, _, _, _, _, _ = GetTradePlayerItemInfo(1)
								if (name == nil) and (bag > -1) and (slot > 0) then
									PickupContainerItem(bag, slot)
									ClickTradeButton(1)
								end
								if not canTrade then
									print("STW: Tradeable " .. cmdarg7 .. " not found in bags.")
								end
							else
								if(cmdarg2 == playerRealm) then
									InitiateTrade(cmdarg1)
								else
									InitiateTrade(cmdarg1.."-"..cmdarg2:gsub("%s+", ""))
								end
							end
						end	
					end)
				end
			end
		-- Add item: (1)ownerName, (2)ownerRealm, (3)ownerClass, (4)itemLink
		elseif(cmd=="A" and (cmdarg1 ~= playerName or cmdarg2 ~= playerRealm)) then
			local newEntry = CreateEntry()
			UpdateEntry(newEntry, cmdarg1, cmdarg2, cmdarg3, cmdarg4, "Open", 180)
			STW_MinimapAnimations:Play()
		-- Delete item: (1)ownerName, (2)ownerRealm, (3)itemLink
		elseif(cmd=="D" and (cmdarg1 ~= playerName or cmdarg2 ~= playerRealm))then
			index = FindEntry(cmdarg1 .. "%" .. cmdarg2 .. "%" .. cmdarg3)
			if index > 0 then
				CloseEntryStatus(entries[index], 300)
			end
		end
	-- chat_msg_system: arg1, arg2 = "message", "sender"
	elseif event == "CHAT_MSG_SYSTEM" then 
		local arg1 = select(1,...)
		local rollName,roll,minRoll,maxRoll = arg1:match("^(.+) rolls (%d+) %((%d+)%-(%d+)%)$")
		if (UnitInParty(rollName) or UnitInRaid(rollName)) then
			if pendingRolls[rollName .. "%" .. minRoll.."%"..maxRoll] ~= nil then
				-- normalize the roll based on the range
				roll = roll - (minRoll - 1)
				local ownerName, _ = UnitName("player")
				local ownerRealm = GetRealmName()
				-- rollName, rollRealm, rollClass, itemLink
				local rollName, rollRealm, rollClass, itemLink = strsplit("%", pendingRolls[rollName .. "%" .. minRoll.."%"..maxRoll])
				UpdateRoll(ownerName.."%"..ownerRealm.."%"..itemLink, rollName.."%"..rollRealm.."%"..rollClass.."%"..roll)
				pendingRolls[rollName .. "%" .. minRoll.."%"..maxRoll] = nil
			end
		end
	elseif event == "PLAYER_REGEN_ENABLED" then
		for i=1,table.getn(pendingButtonCreations) do
			if InCombatLockdown() then
				print("STW: Error, currently in combat.")
				break
			else
				local entry = pendingButtonCreations[1]
				if not entry.targetButton then
					entry.targetButton = AcquireButton(entry)
					entry.targetButton:Show()
				end
				entry.targetButton:SetSize(140, 50)
				entry.targetButton:SetPoint("CENTER")
				entry.targetButton:SetAttribute("type1", "macro")
				entry.targetButton:SetAttribute("macrotext", "/targetexact ")
				-- button mouseover
				entry.targetButton:SetScript('OnEnter', function()
					-- add frame highlight
					entry:SetBackdrop({edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 16})
					entry:SetBackdropBorderColor(1, 1, 0)
				end)
				entry.targetButton:SetScript('OnLeave', function()
					-- remove frame highlight
					entry:SetBackdrop({edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 12})
					entry:SetBackdropBorderColor(1, 1, 1)
				end)	
				-- owner trade button				
				if entry.ownerName and entry.ownerRealm then
					if(entry.ownerRealm == GetRealmName()) then
						entry.targetButton:SetScript('OnMouseUp', function(self, button)
							if button == "RightButton" then
								InitiateTrade(entry.ownerName)
							end
						end)
					else
						entry.targetButton:SetScript('OnMouseUp', function(self, button)
							if button == "RightButton" then
								InitiateTrade(entry.ownerName.."-"..entry.ownerRealm:gsub("%s+", ""))
							end
						end)
					end
				end
				-- secure owner target button
				if entry.ownerName and entry.ownerRealm then
					if entry.ownerRealm == GetRealmName() then
						entry.targetButton:SetAttribute("macrotext", "/targetexact " .. entry.ownerName)
					else
						entry.targetButton:SetAttribute("macrotext", "/targetexact " .. entry.ownerName .. "-" ..entry.ownerRealm:gsub("%s+", ""))
					end
				end
				table.remove(pendingButtonCreations, 1)
			end
		end
		for i=table.getn(pendingDeletion),1,-1 do
			if InCombatLockdown() then
				print("STW: Error, currently in combat.")
				break
			else
				DeleteByItem(pendingDeletion[i])
				table.remove(pendingDeletion, i)
			end
		end
		for i=1,table.getn(pendingButtonUpdates) do
			if InCombatLockdown() then
				print("STW: Error, currently in combat.")
				break
			else
				local entry = pendingButtonUpdates[1]
				-- secure winning target button
				if(entry.realm == GetRealmName()) then
					entry.targetButton:SetAttribute("macrotext", "/targetexact " .. entry.name)
				else
					entry.targetButton:SetAttribute("macrotext", "/targetexact " .. entry.name.."-"..entry.realm:gsub("%s+", ""))
				end
				-- trade winning
				entry.targetButton:SetScript('OnMouseUp', function(self, button)
					if button == "RightButton" then
						if TradeFrame:IsVisible() then
							local canTrade, bag, slot = IsTradeable(entry.itemLink)
							local name, _, _, _, _, _ = GetTradePlayerItemInfo(1)
							if (name == nil) and (bag > -1) and (slot > 0) then
								PickupContainerItem(bag, slot)
								ClickTradeButton(1)
							end
							if not canTrade then
								print("STW: Tradeable " .. entry.itemLink .. " not found in bags.")
							end
						else
							if(entry.realm == GetRealmName()) then
								InitiateTrade(entry.name)
							else
								InitiateTrade(entry.name.."-"..entry.realm:gsub("%s+", ""))
							end
						end
					end	
				end)
				table.remove(pendingButtonUpdates, 1)
			end
		end
	elseif event == "GET_ITEM_INFO_RECEIVED" then
		for itemString, itemLink in pairs(pendingItemLinks) do
			if GetItemInfo(itemLink) then
				local index = FindEntry(itemString)
				if index > 0 then
					local entryFrame = entries[index]
					if entryFrame ~= nil then
						local itemName, _, itemRarity, _, _, itemType,
						itemSubType, _, itemEquipLoc, _, _ =
						GetItemInfo("\"" .. itemLink .. "\"")
						local itemLevel, _, _ = GetDetailedItemLevelInfo(itemLink)
						local _, _, _, _, itemID, _, _, _, _, _, _, _, _, _ = string.find(itemLink,"|?c?f?f?(%x*)|?H?([^:]*):?(%d+):?(%d*):?(%d*):?(%d*):?(%d*):?(%d*):?(%-?%d*):?(%-?%d*):?(%d*):?(%d*):?(%-?%d*)|?h?%[?([^%[%]]*)%]?|?h?|?r?")
						itemID = tonumber(itemID)
						local itemSlot = itemEquipLoc
						-- item color info, name
						if not itemRarity then 
							r, g, b = 1, 1, 1
						elseif itemRarity > 5 or itemRarity < 1 then
							r, g, b = 1, 1, 1
						else
							r, g, b = strsplit(":", itemColors[itemRarity])
						end
						entryFrame.itemEntry.text:SetTextColor(r, g, b)
						entryFrame.itemEntry.text:SetText("[" .. itemName .. "]")
					
						-- item link mouseover
						entryFrame.itemEntry:SetScript('OnEnter', function()
							-- add frame highlight
							entryFrame.itemEntry:SetBackdrop({edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", edgeSize = 16})
							entryFrame.itemEntry:SetBackdropBorderColor(1, 1, 0)
							ItemTooltip1:ClearLines()
							ItemTooltip1:SetHyperlink(itemLink)
							ItemTooltip1:SetPoint("BOTTOMLEFT", entryFrame.itemEntry, "TOPLEFT")
							ItemTooltip1:Show() 
						end)
						-- item link shift modifier (view currently equipped)
						entryFrame.itemEntry:SetScript("OnEvent", function(self, event, key, state)
							if event == "MODIFIER_STATE_CHANGED" and ((key == "LSHIFT") or (key == "RSHIFT")) and entryFrame.itemEntry:IsMouseOver() and menuFrame:IsVisible() then
								if state == 1 then
									if equipSlots[itemSlot] then
										local charItem1, charItem2 = strsplit("%", equipSlots[itemSlot])
										charItem1 = tonumber(charItem1)
										charItem2 = tonumber(charItem2)
										if charItem1 then
											ItemTooltip2:ClearLines()	
											ItemTooltip2:SetHyperlink(GetInventoryItemLink("player", charItem1))
											ItemTooltip2:AddLine("Currently Equipped", .5, 1, 0)
											ItemTooltip2:SetPoint("BOTTOMLEFT", ItemTooltip1, "BOTTOMRIGHT")
											ItemTooltip2:Show()
										end
										if charItem2 then
											ItemTooltip3:ClearLines()	
											ItemTooltip3:SetHyperlink(GetInventoryItemLink("player", charItem2))
											ItemTooltip3:AddLine("Currently Equipped", .5, 1, 0)
											ItemTooltip3:SetPoint("BOTTOMLEFT", ItemTooltip2, "BOTTOMRIGHT")
											ItemTooltip3:Show()
										end
									elseif itemSubType == "Artifact\nRelic" then
										local _, _, relicType0, _ = C_ArtifactUI.GetRelicInfoByItemID(itemID)
										local _, _, relicType1, relicLink1 = C_ArtifactUI.GetEquippedArtifactRelicInfo(1)
										local _, _, relicType2, relicLink2 = C_ArtifactUI.GetEquippedArtifactRelicInfo(2)
										local _, _, relicType3, relicLink3 = C_ArtifactUI.GetEquippedArtifactRelicInfo(3)
										if relicType0 == relicType1 then
											if not ItemTooltip2:IsVisible() then
												ItemTooltip2:ClearLines()	
												ItemTooltip2:SetHyperlink(relicLink1)
												ItemTooltip2:AddLine("Currently Equipped", .5, 1, 0)
												ItemTooltip2:SetPoint("BOTTOMLEFT", ItemTooltip1, "BOTTOMRIGHT")
												ItemTooltip2:Show()
											else
												ItemTooltip3:ClearLines()	
												ItemTooltip3:SetHyperlink(relicLink1)
												ItemTooltip3:AddLine("Currently Equipped", .5, 1, 0)
												ItemTooltip3:SetPoint("BOTTOMLEFT", ItemTooltip2, "BOTTOMRIGHT")
												ItemTooltip3:Show()
											end
										end
										if relicType0 == relicType2 then
											if not ItemTooltip2:IsVisible() then
												ItemTooltip2:ClearLines()	
												ItemTooltip2:SetHyperlink(relicLink2)
												ItemTooltip2:AddLine("Currently Equipped", .5, 1, 0)
												ItemTooltip2:SetPoint("BOTTOMLEFT", ItemTooltip1, "BOTTOMRIGHT")
												ItemTooltip2:Show()
											else
												ItemTooltip3:ClearLines()	
												ItemTooltip3:SetHyperlink(relicLink2)
												ItemTooltip3:AddLine("Currently Equipped", .5, 1, 0)
												ItemTooltip3:SetPoint("BOTTOMLEFT", ItemTooltip2, "BOTTOMRIGHT")
												ItemTooltip3:Show()
											end
										end
										if relicType0 == relicType3 then
											if not ItemTooltip2:IsVisible() then
												ItemTooltip2:ClearLines()	
												ItemTooltip2:SetHyperlink(relicLink3)
												ItemTooltip2:AddLine("Currently Equipped", .5, 1, 0)
												ItemTooltip2:SetPoint("BOTTOMLEFT", ItemTooltip1, "BOTTOMRIGHT")
												ItemTooltip2:Show()
											else
												ItemTooltip3:ClearLines()	
												ItemTooltip3:SetHyperlink(relicLink3)
												ItemTooltip3:AddLine("Currently Equipped", .5, 1, 0)
												ItemTooltip3:SetPoint("BOTTOMLEFT", ItemTooltip2, "BOTTOMRIGHT")
												ItemTooltip3:Show()
											end
										end
									end
								else
									ItemTooltip2:Hide()
									ItemTooltip2:SetOwner(WorldFrame, "ANCHOR_NONE")
									ItemTooltip3:Hide()
									ItemTooltip3:SetOwner(WorldFrame, "ANCHOR_NONE")
								end
							end
						end)					
						
						-- item link click functions
						entryFrame.itemEntry:SetScript('OnMouseUp', function(self, button)
							-- add to chat edit box
							if button == "LeftButton" and IsShiftKeyDown() and ChatFrame1EditBox:HasFocus() then
								ChatFrame1EditBox:Insert(itemLink)
							end
							-- open transmog dressing room
							if button == "LeftButton" and IsControlKeyDown() then
								DressUpItemLink(itemLink)
							end
						end)
					
						-- item level info
						entryFrame.ilvlEntry.text:SetText(itemLevel)
						
						-- formatted item type info
						-- change to "misc." to fit frame
						if itemSubType == "Miscellaneous" then
							itemSubType = "Misc."
						end
						-- modify to fit frame
						if itemSubType == "Artifact Relic" then
							itemSubType = "Artifact\nRelic"
						end
						-- change to "Gem" instead of displaying gem stats
						if itemType == "Gem" and itemSubType ~= "Artifact\nRelic" then
							itemSubType = "Gem"
						end
						-- change to "1H" to fit frame
						if string.find(itemSubType, "One%-Handed") then
							itemSubType, _ = string.gsub(itemSubType, "One%-Handed", "1H")
						end
						-- change to "2H" to fit frame
						if string.find(itemSubType, "Two%-Handed") then
							itemSubType, _ = string.gsub(itemSubType, "Two%-Handed", "2H")
						end
						-- change to "Weapon" to fit frame, instead of super long weapon type
						if itemType ~= "Armor" and itemEquipLoc:len() > 0 then
							itemEquipLoc = "Inv_Weapon"
						end
						-- if cloak, do not list as cloth for clarity
						if itemEquipLoc == "INVTYPE_CLOAK" then
							itemSubType = "Misc."
						end
						-- format armor slot name and capitalize first letter, enter data
						if itemEquipLoc:len() > 0 then
							_, itemEquipLoc = strsplit("_", itemEquipLoc)
							itemEquipLoc = string.lower(itemEquipLoc)
							itemEquipLoc = itemEquipLoc:gsub("^%l", string.upper)
							entryFrame.typeEntry.text:SetText(itemSubType .. "\n" .. itemEquipLoc)
						else
							entryFrame.typeEntry.text:SetText(itemSubType)
						end
						pendingItemLinks[itemString] = nil
					end
				end
			end
		end
	elseif event == "LOADING_SCREEN_ENABLED" then
		if loadtime == 0 then
			loadtime = time()
		end
	elseif event == "LOADING_SCREEN_DISABLED" then
		if loadtime ~= 0 then
			loadtime = time() - loadtime
			for i=1, table.getn(entries), 1 do
				AdjustTimer(entries[i], loadtime)
			end
			loadtime = 0
		end
	elseif event == "ADDON_LOADED" and arg1 == "ShareTheWealth" then	
		-- Load saved or default variables
		STW_ConfigFrame_Cancel()
		
		-- Variables loaded, init main window
		InitMain()
		
		-- Minimap button click
		STW_MinimapButton:SetScript("OnClick", function(self,button)
			if InCombatLockdown() then
				print("STW: Error, currently in combat.")
				return
			end	
			if button == "LeftButton" then
				if menuFrame:IsVisible() then
					menuFrame:Hide()
				else
					menuFrame:Show()
				end
			elseif button == "RightButton" then
				 InterfaceOptionsFrame_OpenToCategory(STW_ConfigFrame.name)
				 InterfaceOptionsFrame_OpenToCategory(STW_ConfigFrame.name)
			end
		end)
	end
end)
eventFrame:RegisterEvent("ADDON_LOADED")
eventFrame:RegisterEvent("CHAT_MSG_SYSTEM")
eventFrame:RegisterEvent("CHAT_MSG_ADDON")
eventFrame:RegisterEvent("PLAYER_REGEN_ENABLED")
eventFrame:RegisterEvent("LOADING_SCREEN_ENABLED")
eventFrame:RegisterEvent("LOADING_SCREEN_DISABLED")
eventFrame:RegisterEvent("GET_ITEM_INFO_RECEIVED")
C_ChatInfo.RegisterAddonMessagePrefix(addonPrefix)
