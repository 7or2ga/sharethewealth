# Share the Wealth

## Getting Started
---
Clone a copy of the Share the Wealth master branch, and get reading!

* Highly recommend becoming familiar with the WoW API beforehand. Many references are available on the internet. 
 * http://wowwiki.wikia.com/wiki/Getting\_started\_with\_writing\_AddOns is a good place to start.
 * http://wowprogramming.com/ is a great reference for the API
 * https://www.wowinterface.com/forums/ is a great forum for questions, and getting tips from experienced addon developers that are much smarter than Snocones22

### Prerequisites
No library prerequisites! This code is 100% dependency-free.

* The ability to read code spaghetti might be helpful, this addon was unfortunately not written with maintainability in mind.
* A solid LUA / text editor is recommended. https://notepad-plus-plus.org/ is fantastic.

## Testing
---
A testing method needs to be developed. Chaotic button mashing is not an effective test solution.

* For now, solo testing in a group with a free WoW starter account is possible.
* Having a guinea pig raid group is also helpful.

## Installation
---
Manual installaton instructions can be found here: https://support.curse.com/hc/en-us/articles/204270005--WoW-Manually-Installing-AddOns

For automated addon installation of alpha, beta, and full version relases, the Twitch client is recommended.

## Contributing
---
Contact Snocones22 for information on contributing! Getting additional help was never considered to be likely, though it would not be ruled out.

## Versioning
---
Example: v1.2.3

* Major overhauls will increment the first decimal location, the "1", and set succeeding decimal locations to "0"
* Feature additions or critical fixes will increment the second decimal location, the "2", and set succeeding decimal locations to "0".
* Small bug fixes and minor releases will increment the third decimal location, the "3".

## Authors
---
* Snocones22 aka Viktorya-KulTiras :: Original project creator

See also the list of contributors who participated in this project.

## Contributors
---
* None yet!


## License
---
Licensed under the "Creative Commons Attribution - NonCommercial 3.0 Unported" license.

More information here: https://creativecommons.org/licenses/by-nc/3.0/

## Acknowledgments
---
* Special thanks to the Rainbow Team Alpha guild on Kul Tiras, for putting up with testing and constant requests for feedback.
* Thanks to any resources mentioned on this page, for helping those seeking to provide fun and useful tools to the community.