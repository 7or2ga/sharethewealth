## Interface: 80000
## Title: ShareTheWealth
## Version: 1.0.12
## Notes: Track rolls for tradeable loot within your group.
## Author: Viktorya-KulTiras-US aka Snohcones-KulTiras-US
##SavedVariablesPerCharacter: STW_Config
ShareTheWealth.xml
ShareTheWealth.lua
